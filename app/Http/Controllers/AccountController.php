<?php

namespace App\Http\Controllers;

//use Cartalyst\Sentinel\Native\Facades\Sentinel;
use App\Products;
use Illuminate\Support\Facades\DB;
use Sentinel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class AccountController extends Controller
{
    public function index()
    {
        if (!Sentinel::check()) {
            return view('account.index');
        } else {
            $user = Sentinel::check();
            $orders = DB::table('order_products')
                ->where('user_id', $user->id)
                ->get();
            return view('account.profile')->with('user', $user)->with('orders', $orders);
        }
    }

    public function postLogin()
    {
        $credentials = [
            'email' => $_POST['login_email'],
            'password' => $_POST['login_password'],
        ];

        $user = Sentinel::authenticate($credentials);
        try {
            if ($user) {
                Sentinel::login($user);
                return Redirect::route("home");
            } else
                return Redirect::route("account")->withErrors(['Invalid email/password!']);
        } catch (UserExistsException $e) {
            return Redirect::route("account")->withErrors(['Unknown Error. Please contact administrator!']);
        }
    }

    public function getLogin()
    {
        if (!Sentinel::check()) {
            return view('account.index');
        } else
            return view('account.profile');
    }

    public function getLogout()
    {
        Sentinel::logout();
        return view('account.index');
    }

    public function postRegister()
    {

        $credentials = [
            'first_name' => $_POST['register_name'],
            'email' => $_POST['register_email'],
            'password' => $_POST['register_password'],
        ];

        Sentinel::registerAndActivate($credentials);

        return redirect('account')->with('message', 'Register success. Please login!');
    }

    public function getRegister()
    {
        var_dump('delfrinando');
        exit;
    }
}
