<?php

namespace App\Http\Controllers;

use App\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hajjs = Products::where('category_id', '=', 1)->take(8)->get();
        $umrahs = Products::where('category_id', '=', 2)->take(8)->get();
        $halalfoods = Products::where('category_id', '=', 3)->take(8)->get();
        $clothes = Products::where('category_id', '=', 4)->take(8)->get();

        return view('index')->with(compact('hajjs', 'umrahs', 'halalfoods', 'clothes'));
    }

    public function tampil()
    {
        $delfrinando = DB::table('users')->get();

        print_r($delfrinando);
    }
}
