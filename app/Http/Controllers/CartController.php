<?php

namespace App\Http\Controllers;

//use Gloudemans\Shoppingcart\Facades\Cart;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use League\Flysystem\Exception;
use Sentinel;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Redirect;

class CartController extends Controller
{
    public function index()
    {
        $carts = Cart::content();
        $total = Cart::subtotal();
        return view('cart.index', compact('carts', 'total'));
    }

    public function addcart()
    {
//        var_dump($_REQUEST);
        $id = $_POST['id'];
        $name = $_POST['name'];
        $qty = $_POST['qty'];
        $price = $_POST['price'];

        Cart::add($id, $name, $qty, $price);

        return Redirect::route("cart");
    }

    public function konfirmasipembayaran()
    {
        $order_id = $_POST['order_id'];

        if ($order_id === 0) {
            return view('account');
        } else {
            $order = [
                'updated_at' => Carbon::now()->toDateTimeString(),
                'status' => 2
            ];
            DB::table('order_products')->where('id', $order_id)->update($order);

            return Redirect::route("account");
        }
    }

    public function deletecart($rowId)
    {
        Cart::remove($rowId);

        return Redirect::route("cart");
    }

    public function checkout()
    {
        if (!Sentinel::check()) {
            return view('account.index');
        } else {
            $carts = Cart::content();
            $total = Cart::subtotal();
            return view('cart.checkout', compact('carts', 'total'));
        }

    }

    public function postcheckout()
    {
        if ($user = Sentinel::check()) {
//            var_dump($user->email);
//            exit;

            $carts = Cart::content();

            $email = $_POST['email'];
            $name = $_POST['name'];
            $phone = $_POST['phone'];
            $address = $_POST['address'];
            $paymentmethod = $_POST['paymentmethod'];
            $total = Cart::subtotal(number_format(2), '.', '');
            $status = 1;

            $order = [
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
                'user_id' => $user->id,
                'amount' => $total,
                'status' => $status
            ];

            try {
                $order_id = DB::table('order_products')->insertGetId(
                    $order
                );

                foreach ($carts as $cart) {

//                    if($cart->qty > 1){
                    for ($i = 0; $i < $cart->qty; $i++) {
                        $order_detail = [
                            'created_at' => Carbon::now()->toDateTimeString(),
                            'updated_at' => Carbon::now()->toDateTimeString(),
                            'order_id' => $order_id,
                            'category_id' => 1,
                            'price' => $cart->price
                        ];

                        DB::table('order_detail')->insert($order_detail);
                    }
//                    }
                }

                Cart::destroy();
            } catch (Exception $e) {
            }

            return Redirect::route("account");

        }

    }


    public function getdatajamaah($order_detail_id = 0)
    {
        if ($order_detail_id === 0) {
            return view('cart');
        } else {
            $order = DB::table('order_detail')
                ->where('id', $order_detail_id)
                ->first();

            return view('cart/datajamaah', compact('order'));
        }
    }

    public function postdatajamaah()
    {
//        var_dump('awalll');
        var_dump($_POST);
//        $data = Request::file('filefield');

//        var_dump(Input::file('scanktp')->getClientOriginalExtension());
        exit;

//        $this->validate($data, [
//            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
//        ]);

        $imageName = 'delfrinandpfoto.' . $data->getClientOriginalExtension();
        $data->move(public_path('images'), $imageName);

        var_dump('deflraindn');
//        return view('/datajamaah');
    }
}
