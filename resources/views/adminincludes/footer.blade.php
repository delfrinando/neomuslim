<footer class="row">
    <p class="col-md-9 col-sm-9 col-xs-12 copyright">&copy; <a href="http://delfrinando.com" target="_blank">Delfrnando Pranata</a> 2016</p>

    <p class="col-md-3 col-sm-3 col-xs-12 powered-by">Powered by: <a
                href="http://delfrinando.com">Delfrinando Pranata</a></p>
</footer>