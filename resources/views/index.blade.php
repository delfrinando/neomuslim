@extends('layouts.default')
@section('content')

        <!-- Start Promo section -->
{{--<section id="aa-promo">--}}
    {{--<div class="container">--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-12">--}}
                {{--<div class="aa-promo-area">--}}
                    {{--<div class="row">--}}
                        {{--<!-- promo left -->--}}
                        {{--<div class="col-md-5 no-padding">--}}
                            {{--<div class="aa-promo-right">--}}
                                {{--<div class="aa-promo-banner">--}}
                                    {{--<img src="{{URL::asset('img/promo-banner-1.jpg')}}" alt="img">--}}

                                    {{--<div class="aa-prom-content">--}}
                                        {{--<span><a href="/product?cat=1">HAJJ</a></span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<!-- promo right -->--}}
                        {{--<div class="col-md-7 no-padding">--}}
                            {{--<div class="aa-promo-right">--}}
                                {{--<div class="aa-single-promo-right">--}}
                                    {{--<div class="aa-promo-banner">--}}
                                        {{--<img src="{{URL::asset('img/promo-banner-3.jpg')}}" alt="img">--}}

                                        {{--<div class="aa-prom-content">--}}
                                            {{--<span><a href="/product?cat=2">UMRAH</a></span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="aa-single-promo-right">--}}
                                    {{--<div class="aa-promo-banner">--}}
                                        {{--<img src="{{URL::asset('img/promo-banner-2.jpg')}}" alt="img">--}}

                                        {{--<div class="aa-prom-content">--}}
                                            {{--<span><a href="#">IFAQ & SADAQAH</a></span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="aa-single-promo-right">--}}
                                    {{--<div class="aa-promo-banner">--}}
                                        {{--<img src="{{URL::asset('img/promo-banner-4.jpg')}}" alt="img">--}}

                                        {{--<div class="aa-prom-content">--}}
                                            {{--<span><a href="/product?cat=3">HALAL FOOD</a></span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="aa-single-promo-right">--}}
                                    {{--<div class="aa-promo-banner">--}}
                                        {{--<img src="{{URL::asset('img/promo-banner-5.jpg')}}" alt="img">--}}

                                        {{--<div class="aa-prom-content">--}}
                                            {{--<span><a href="/product?cat=4">CLOTHES</a></span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="aa-single-promo-right">--}}
                                    {{--<div class="aa-promo-banner">--}}
                                        {{--<img src="{{URL::asset('img/promo-banner-5.jpg')}}" alt="img">--}}

                                        {{--<div class="aa-prom-content">--}}
                                            {{--<span><a href="/product?cat=4">CLOTHES</a></span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="aa-single-promo-right">--}}
                                    {{--<div class="aa-promo-banner">--}}
                                        {{--<img src="{{URL::asset('img/promo-banner-5.jpg')}}" alt="img">--}}

                                        {{--<div class="aa-prom-content">--}}
                                            {{--<span><a href="/product?cat=4">CLOTHES</a></span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</section>--}}
<!-- / Promo section -->
<!-- Products section -->
<section id="aa-product">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="aa-product-area">
                        <div class="aa-product-inner">
                            <!-- start prduct navigation -->
                            <ul class="nav nav-tabs aa-products-tab">
                                <li class="active"><a href="#hajj" data-toggle="tab">Hajj</a></li>
                                <li><a href="#umrah" data-toggle="tab">Umrah</a></li>
                                <li><a href="#halalfood" data-toggle="tab">Halal Food</a></li>
                                <li><a href="#clothes" data-toggle="tab">Clothes</a></li>
                                {{--<li><a href="#electronics" data-toggle="tab">Infaq & Sadaqah</a></li>--}}
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <!-- Start men product category -->
                                <div class="tab-pane fade in active" id="hajj">
                                    <ul class="aa-product-catg">
                                        <!-- start single product m -->
                                        @foreach($hajjs as $hajj)
                                            <li>
                                                <figure>

                                                    <form action="/cart/add" method="POST">
                                                        {!! csrf_field() !!}
                                                        <input type="hidden" name="id" value="{{ $hajj->id }}">
                                                        <input type="hidden" name="name" value="{{ $hajj->name }}">
                                                        <input type="hidden" name="price" value="{{ $hajj->price }}">
                                                        <input type="hidden" name="qty" value=1>
                                                        <a class="aa-product-img" href="#"><img
                                                                    src="{{URL::asset('img/haji/1.jpg')}}"
                                                                    alt="polo shirt img"></a>

                                                        <a class="aa-add-card-btn" href="#"
                                                           onclick="$(this).closest('form').submit()">
                                                            <span type="submit" class="fa fa-shopping-cart"></span>Add
                                                            To Cart</a>

                                                        {{--<div class="aa-add-card-btn">--}}
                                                        {{--<a class="aa-add-to-cart-btn" href="#">Wishlist</a>--}}
                                                        {{--<input type="submit" class="aa-add-to-cart-btn"--}}
                                                        {{--value="Add to Cart">--}}
                                                        {{--</div>--}}
                                                        {{--<input type="submit"--}}
                                                        {{--class="fa fa-shopping-cart aa-add-card-btn"--}}
                                                        {{--value="Add to Cart">--}}
                                                        {{--<input type="submit"--}}
                                                        {{--class="fa fa-shopping-cart aa-add-card-btn"--}}
                                                        {{--value="Wishlist">--}}
                                                    </form>
                                                    <figcaption>
                                                        <h4 class="aa-product-title"><a href="#">{{$hajj->name}}</a>
                                                        </h4>
                                                        <span class="aa-product-price">Rp. {{$hajj->price}}</span></span>

                                                        <p class="aa-product-descrip">{{$hajj->description}}</p>
                                                    </figcaption>
                                                </figure>
                                                <div class="aa-product-hvr-content">
                                                    <a href="#" data-toggle2="tooltip" data-placement="top"
                                                       title="Quick View"
                                                       data-toggle="modal"
                                                       data-target="#quick-view-modal-{{$hajj->id}}"><span
                                                                class="fa fa-search"></span></a>
                                                    <a href="#" data-toggle="tooltip" data-placement="top"
                                                       title="Add to Wishlist"><span class="fa fa-heart-o"></span></a>

                                                </div>
                                                <!-- product badge -->
                                                <span class="aa-badge aa-sale" href="#">SALE!</span>
                                            </li>
                                            @endforeach
                                                    <!-- start single product item -->
                                    </ul>
                                    <a class="aa-browse-btn" href="/product?cat=1">Browse all Product <span
                                                class="fa fa-long-arrow-right"></span></a>
                                </div>
                                <!-- / men product category -->
                                <!-- start women product category -->
                                <div class="tab-pane fade" id="umrah">
                                    <ul class="aa-product-catg">
                                        <!-- start single product item -->
                                        @foreach($umrahs as $umrah)
                                            <li>
                                                <figure>

                                                    <form action="/cart/add" method="POST">
                                                        {!! csrf_field() !!}
                                                        <input type="hidden" name="id" value="{{ $umrah->id }}">
                                                        <input type="hidden" name="name" value="{{ $umrah->name }}">
                                                        <input type="hidden" name="price" value="{{ $umrah->price }}">
                                                        <input type="hidden" name="qty" value=1>
                                                        <a class="aa-product-img" href="#"><img
                                                                    src="{{URL::asset('img/umrah/1.jpg')}}"
                                                                    alt="polo shirt img"></a>
                                                        <a class="aa-add-card-btn" href="#"
                                                           onclick="$(this).closest('form').submit()">
                                                            <span type="submit" class="fa fa-shopping-cart"></span>Add
                                                            To Cart</a>
                                                    </form>
                                                    <figcaption>
                                                        <h4 class="aa-product-title"><a href="#">{{$umrah->name}}</a>
                                                        </h4>
                                                        <span class="aa-product-price">Rp. {{$umrah->price}}</span></span>

                                                        <p class="aa-product-descrip">{{$umrah->description}}</p>
                                                    </figcaption>
                                                </figure>
                                                <div class="aa-product-hvr-content">
                                                    <a href="#" data-toggle2="tooltip" data-placement="top"
                                                       title="Quick View"
                                                       data-toggle="modal"
                                                       data-target="#quick-view-modal-{{$umrah->id}}"><span
                                                                class="fa fa-search"></span></a>
                                                    <a href="#" data-toggle="tooltip" data-placement="top"
                                                       title="Add to Wishlist"><span class="fa fa-heart-o"></span></a>
                                                </div>
                                                <!-- product badge -->
                                                <span class="aa-badge aa-sale" href="#">SALE!</span>
                                            </li>
                                            @endforeach
                                                    <!-- start single product item -->
                                    </ul>
                                    <a class="aa-browse-btn" href="/product?cat=2">Browse all Product <span
                                                class="fa fa-long-arrow-right"></span></a>
                                </div>
                                <!-- / women product category -->
                                <!-- start sports product category -->
                                <div class="tab-pane fade" id="halalfood">
                                    <ul class="aa-product-catg">
                                        <!-- start single product item -->
                                        @foreach($halalfoods as $halalfood)
                                            <li>
                                                <figure>

                                                    <form action="/cart/add" method="POST">
                                                        {!! csrf_field() !!}
                                                        <input type="hidden" name="id" value="{{ $halalfood->id }}">
                                                        <input type="hidden" name="name" value="{{ $halalfood->name }}">
                                                        <input type="hidden" name="price"
                                                               value="{{ $halalfood->price }}">
                                                        <input type="hidden" name="qty" value=1>
                                                        <a class="aa-product-img" href="#"><img
                                                                    src="{{URL::asset('img/women/girl-1.png')}}"
                                                                    alt="polo shirt img"></a>
                                                        <a class="aa-add-card-btn" href="#"
                                                           onclick="$(this).closest('form').submit()">
                                                            <span type="submit" class="fa fa-shopping-cart"></span>Add
                                                            To Cart</a>
                                                    </form>
                                                    <figcaption>
                                                        <h4 class="aa-product-title"><a
                                                                    href="#">{{$halalfood->name}}</a></h4>
                                                        <span class="aa-product-price">Rp. {{$halalfood->price}}</span></span>

                                                        <p class="aa-product-descrip">{{$halalfood->description}}</p>
                                                    </figcaption>
                                                </figure>
                                                <div class="aa-product-hvr-content">
                                                    <a href="#" data-toggle2="tooltip" data-placement="top"
                                                       title="Quick View"
                                                       data-toggle="modal"
                                                       data-target="#quick-view-modal-{{$halalfood->id}}"><span
                                                                class="fa fa-search"></span></a>
                                                </div>
                                                <!-- product badge -->
                                                <span class="aa-badge aa-sale" href="#">SALE!</span>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <a class="aa-browse-btn" href="/product?cat=3">Browse all Product <span
                                                class="fa fa-long-arrow-right"></span></a>
                                </div>
                                <!-- / sports product category -->
                                <!-- start electronic product category -->
                                <div class="tab-pane fade" id="clothes">
                                    <ul class="aa-product-catg">
                                        <!-- start single product item -->
                                        @foreach($clothes as $clothe)
                                            <li>
                                                <figure>

                                                    <form action="/cart/add" method="POST">
                                                        {!! csrf_field() !!}
                                                        <input type="hidden" name="id" value="{{ $clothe->id }}">
                                                        <input type="hidden" name="name" value="{{ $clothe->name }}">
                                                        <input type="hidden" name="price" value="{{ $clothe->price }}">
                                                        <input type="hidden" name="qty" value=1>
                                                        <a class="aa-product-img" href="#"><img
                                                                    src="{{URL::asset('img/women/girl-1.png')}}"
                                                                    alt="polo shirt img"></a>
                                                        <a class="aa-add-card-btn" href="#"
                                                           onclick="$(this).closest('form').submit()">
                                                            <span type="submit" class="fa fa-shopping-cart"></span>Add
                                                            To Cart</a>
                                                    </form>
                                                    <figcaption>
                                                        <h4 class="aa-product-title"><a href="#">{{$clothe->name}}</a>
                                                        </h4>
                                                        <span class="aa-product-price">Rp. {{$clothe->price}}</span></span>

                                                        <p class="aa-product-descrip">{{$clothe->description}}</p>
                                                    </figcaption>
                                                </figure>
                                                <div class="aa-product-hvr-content">
                                                    <a href="#" data-toggle2="tooltip" data-placement="top"
                                                       title="Quick View"
                                                       data-toggle="modal"
                                                       data-target="#quick-view-modal-{{$clothe->id}}"><span
                                                                class="fa fa-search"></span></a>
                                                </div>
                                                <!-- product badge -->
                                                <span class="aa-badge aa-sale" href="#">SALE!</span>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <a class="aa-browse-btn" href="/product?cat=4">Browse all Product <span
                                                class="fa fa-long-arrow-right"></span></a>
                                </div>
                                <!-- / electronic product category -->
                            </div>


                            @foreach($hajjs as $hajj)
                                <div class="modal fade" id="quick-view-modal-{{$hajj->id}}" tabindex="-1" role="dialog"
                                     aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-hidden="true">&times;</button>
                                                <div class="row">
                                                    <!-- Modal view slider -->
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <div class="aa-product-view-slider">
                                                            <div class="simpleLens-gallery-container" id="demo-1">
                                                                <div class="simpleLens-container">
                                                                    <div class="simpleLens-big-image-container">
                                                                        <img src="{{URL::asset('img/women/girl-1.png')}}"
                                                                             class="simpleLens-big-image">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Modal view content -->
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <div class="aa-product-view-content">
                                                            <h3>{{$hajj->name}}</h3>

                                                            <div class="aa-price-block">
                                                                <span class="aa-product-view-price">{{$hajj->price}}</span>

                                                                <p class="aa-product-avilability">Avilability:
                                                                    <span>{{$hajj->stock}}</span></p>
                                                            </div>
                                                            <p>{{$hajj->description}}</p>

                                                            <div class="aa-prod-quantity">
                                                                <form action="">
                                                                    <select name="" id="">
                                                                        <option value="0" selected="1">1</option>
                                                                        <option value="1">2</option>
                                                                        <option value="2">3</option>
                                                                        <option value="3">4</option>
                                                                        <option value="4">5</option>
                                                                        <option value="5">6</option>
                                                                    </select>
                                                                </form>
                                                            </div>
                                                            <br>

                                                            <div class="aa-prod-view-bottom">
                                                                <table>
                                                                    <tr>

                                                                        <td>
                                                                            <form action="/cart/add" method="POST">
                                                                                {!! csrf_field() !!}
                                                                                <input type="hidden" name="id"
                                                                                       value="{{ $hajj->id }}">
                                                                                <input type="hidden" name="name"
                                                                                       value="{{ $hajj->name }}">
                                                                                <input type="hidden" name="price"
                                                                                       value="{{ $hajj->price }}">
                                                                                <input type="hidden" name="qty" value=1>
                                                                                <a href="#" class="aa-add-to-cart-btn"
                                                                                   onclick="$(this).closest('form').submit()"><span
                                                                                            class="fa fa-shopping-cart"></span>Add
                                                                                    To
                                                                                    Cart</a>
                                                                            </form>
                                                                        </td>
                                                                        <td>
                                                                            <a href="{{ URL::to('product/' . $hajj->id) }}"
                                                                               class="aa-add-to-cart-btn">View
                                                                                Details</a>
                                                                        </td>
                                                                    </tr>
                                                                </table>


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div>
                            @endforeach


                            @foreach($umrahs as $umrah)
                                <div class="modal fade" id="quick-view-modal-{{$umrah->id}}" tabindex="-1" role="dialog"
                                     aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-hidden="true">&times;</button>
                                                <div class="row">
                                                    <!-- Modal view slider -->
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <div class="aa-product-view-slider">
                                                            <div class="simpleLens-gallery-container" id="demo-1">
                                                                <div class="simpleLens-container">
                                                                    <div class="simpleLens-big-image-container">
                                                                        <img src="{{URL::asset('img/umrah/1.jpg')}}"
                                                                             class="simpleLens-big-image">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Modal view content -->
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <div class="aa-product-view-content">
                                                            <h3>{{$umrah->name}}</h3>

                                                            <div class="aa-price-block">
                                                                <span class="aa-product-view-price">{{$umrah->price}}</span>

                                                                <p class="aa-product-avilability">Avilability:
                                                                    <span>{{$umrah->stock}}</span></p>
                                                            </div>
                                                            <p>{{$umrah->description}}</p>

                                                            <div class="aa-prod-quantity">
                                                                <form action="">
                                                                    <select name="" id="">
                                                                        <option value="0" selected="1">1</option>
                                                                        <option value="1">2</option>
                                                                        <option value="2">3</option>
                                                                        <option value="3">4</option>
                                                                        <option value="4">5</option>
                                                                        <option value="5">6</option>
                                                                    </select>
                                                                </form>
                                                            </div>
                                                            <br>

                                                            <div class="aa-prod-view-bottom">
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <form action="/cart/add" method="POST">
                                                                                {!! csrf_field() !!}
                                                                                <input type="hidden" name="id"
                                                                                       value="{{ $umrah->id }}">
                                                                                <input type="hidden" name="name"
                                                                                       value="{{ $umrah->name }}">
                                                                                <input type="hidden" name="price"
                                                                                       value="{{ $umrah->price }}">
                                                                                <input type="hidden" name="qty" value=1>
                                                                                <a href="#" class="aa-add-to-cart-btn"
                                                                                   onclick="$(this).closest('form').submit()"><span
                                                                                            class="fa fa-shopping-cart"></span>Add
                                                                                    To
                                                                                    Cart</a>
                                                                            </form>
                                                                        </td>
                                                                        <td>
                                                                            <a href="{{ URL::to('product/' . $umrah->id) }}"
                                                                               class="aa-add-to-cart-btn">View
                                                                                Details</a>
                                                                        </td>
                                                                    </tr>
                                                                </table>


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div>
                            @endforeach


                            @foreach($halalfoods as $halalfood)
                                <div class="modal fade" id="quick-view-modal-{{$halalfood->id}}" tabindex="-1"
                                     role="dialog"
                                     aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-hidden="true">&times;</button>
                                                <div class="row">
                                                    <!-- Modal view slider -->
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <div class="aa-product-view-slider">
                                                            <div class="simpleLens-gallery-container" id="demo-1">
                                                                <div class="simpleLens-container">
                                                                    <div class="simpleLens-big-image-container">
                                                                        <img src="{{URL::asset('img/women/girl-1.png')}}"
                                                                             class="simpleLens-big-image">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Modal view content -->
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <div class="aa-product-view-content">
                                                            <h3>{{$halalfood->name}}</h3>

                                                            <div class="aa-price-block">
                                                                <span class="aa-product-view-price">{{$halalfood->price}}</span>

                                                                <p class="aa-product-avilability">Avilability:
                                                                    <span>{{$halalfood->stock}}</span></p>
                                                            </div>
                                                            <p>{{$halalfood->description}}</p>

                                                            <div class="aa-prod-quantity">
                                                                <form action="">
                                                                    <select name="" id="">
                                                                        <option value="0" selected="1">1</option>
                                                                        <option value="1">2</option>
                                                                        <option value="2">3</option>
                                                                        <option value="3">4</option>
                                                                        <option value="4">5</option>
                                                                        <option value="5">6</option>
                                                                    </select>
                                                                </form>
                                                            </div>
                                                            <br>

                                                            <div class="aa-prod-view-bottom">
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <form action="/cart/add" method="POST">
                                                                                {!! csrf_field() !!}
                                                                                <input type="hidden" name="id"
                                                                                       value="{{ $halalfood->id }}">
                                                                                <input type="hidden" name="name"
                                                                                       value="{{ $halalfood->name }}">
                                                                                <input type="hidden" name="price"
                                                                                       value="{{ $halalfood->price }}">
                                                                                <input type="hidden" name="qty" value=1>
                                                                                <a href="#" class="aa-add-to-cart-btn"
                                                                                   onclick="$(this).closest('form').submit()"><span
                                                                                            class="fa fa-shopping-cart"></span>Add
                                                                                    To
                                                                                    Cart</a>
                                                                            </form>
                                                                        </td>
                                                                        <td>
                                                                            <a href="{{ URL::to('product/' . $halalfood->id) }}"
                                                                               class="aa-add-to-cart-btn">View
                                                                                Details</a>
                                                                        </td>
                                                                    </tr>
                                                                </table>


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div>
                            @endforeach


                            @foreach($clothes as $clothe)
                                <div class="modal fade" id="quick-view-modal-{{$clothe->id}}" tabindex="-1"
                                     role="dialog"
                                     aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-hidden="true">&times;</button>
                                                <div class="row">
                                                    <!-- Modal view slider -->
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <div class="aa-product-view-slider">
                                                            <div class="simpleLens-gallery-container" id="demo-1">
                                                                <div class="simpleLens-container">
                                                                    <div class="simpleLens-big-image-container">
                                                                        <img src="{{URL::asset('img/women/girl-1.png')}}"
                                                                             class="simpleLens-big-image">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Modal view content -->
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <div class="aa-product-view-content">
                                                            <h3>{{$clothe->name}}</h3>

                                                            <div class="aa-price-block">
                                                                <span class="aa-product-view-price">{{$clothe->price}}</span>

                                                                <p class="aa-product-avilability">Avilability:
                                                                    <span>{{$clothe->stock}}</span></p>
                                                            </div>
                                                            <p>{{$clothe->description}}</p>

                                                            <div class="aa-prod-quantity">
                                                                <form action="">
                                                                    <select name="" id="">
                                                                        <option value="0" selected="1">1</option>
                                                                        <option value="1">2</option>
                                                                        <option value="2">3</option>
                                                                        <option value="3">4</option>
                                                                        <option value="4">5</option>
                                                                        <option value="5">6</option>
                                                                    </select>
                                                                </form>
                                                            </div>
                                                            <br>

                                                            <div class="aa-prod-view-bottom">
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <form action="/cart/add" method="POST">
                                                                                {!! csrf_field() !!}
                                                                                <input type="hidden" name="id"
                                                                                       value="{{ $clothe->id }}">
                                                                                <input type="hidden" name="name"
                                                                                       value="{{ $clothe->name }}">
                                                                                <input type="hidden" name="price"
                                                                                       value="{{ $clothe->price }}">
                                                                                <input type="hidden" name="qty" value=1>
                                                                                <a href="#" class="aa-add-to-cart-btn"
                                                                                   onclick="$(this).closest('form').submit()"><span
                                                                                            class="fa fa-shopping-cart"></span>Add
                                                                                    To
                                                                                    Cart</a>
                                                                            </form>
                                                                        </td>
                                                                        <td>
                                                                            <a href="{{ URL::to('product/' . $clothe->id) }}"
                                                                               class="aa-add-to-cart-btn">View
                                                                                Details</a>
                                                                        </td>
                                                                    </tr>
                                                                </table>


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- / Products section -->


@stop