@extends('layouts.subdefault')
@section('content')
{{--<p>Date: <input type="text" id="datepicker"></p>--}}
        <!-- Cart view section -->

<section id="checkout">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="checkout-area">
                    <form action="/cart/datajamaah" method="POST" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <input type="hidden" name="order_detail_id" value="{{ $order->id }}">

                        <div class="row">
                            <div class="col-md-8">
                                <div class="checkout-left">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default aa-checkout-billaddress">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion"
                                                       href="#collapseThree">
                                                        Data Jamaah
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseThree" class="panel-collapse">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="aa-checkout-single-bill">
                                                                <input name="ktp" type="text" placeholder="No KTP">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="aa-checkout-single-bill">
                                                                <input name="passport" type="text"
                                                                       placeholder="No Passport">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="aa-checkout-single-bill">
                                                                <input name="namalengkap" type="text"
                                                                       placeholder="Nama Lengkap">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="aa-checkout-single-bill">
                                                                <input name="namaayahkandung" type="text"
                                                                       placeholder="Nama Ayah Kandung">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="aa-checkout-single-bill">
                                                                <input name="tempattanggallahir" type="text"
                                                                       placeholder="Tempat dan Tanggal Lahir">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="aa-checkout-single-bill">
                                                                <input name="email" type="email"
                                                                       placeholder="Email Address*">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="aa-checkout-single-bill">
                                                                <input name="phone" type="tel" placeholder="Phone*">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="aa-checkout-single-bill">
                                                                <textarea name="address" cols="8" rows="3"
                                                                          placeholder="Address*"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="aa-checkout-single-bill">
                                                                <input name="kelurahan" type="text"
                                                                       placeholder="Desa /Kelurahan">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="aa-checkout-single-bill">
                                                                <input name="kecamatan" type="text"
                                                                       placeholder="Kecamatan">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="aa-checkout-single-bill">
                                                                <input name="kota" type="text"
                                                                       placeholder="Kabupaten /Kota">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="aa-checkout-single-bill">
                                                                <input name="propinsi" type="text"
                                                                       placeholder="Propinsi">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="aa-checkout-single-bill">
                                                                <input name="propinsi" type="text"
                                                                       placeholder="Kode Pos">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="aa-checkout-single-bill">
                                                                <input disabled="disabled"
                                                                       placeholder="Scan KTP">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="aa-checkout-single-bill">
                                                                <input name="scanktp" type="file">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="aa-checkout-single-bill">
                                                                <input disabled="disabled"
                                                                       placeholder="Scan Passport">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="aa-checkout-single-bill">
                                                                <input name="scanpassport" type="file">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="aa-checkout-single-bill">
                                                                <input disabled="disabled"
                                                                       placeholder="Foto">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="aa-checkout-single-bill">
                                                                <input name="foto" type="file">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="aa-checkout-single-bill">
                                                                <input disabled="disabled"
                                                                       placeholder="Gender">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class=>
                                                                <input type="radio" id="male" name="gender" checked="">
                                                                Laki-laki <br>
                                                                <input type="radio" id="male" name="gender" checked="">
                                                                Perempuan
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="aa-checkout-single-bill">
                                                                <button type="submit" class="btn btn-success">Save
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@stop