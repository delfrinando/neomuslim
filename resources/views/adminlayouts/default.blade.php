<!doctype html>
<html>
<head>
    @include('adminincludes.head')
</head>
<body>
@include('adminincludes.topmenu')
<div class="ch-container">
    <div class="row">
        @include('adminincludes.leftmenu')
        @yield('contentadmin')
    </div>
    @include('adminincludes.footer')

</div>
</body>
</html>