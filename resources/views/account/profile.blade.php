@extends('layouts.subdefault')
@section('content')
        <!-- Products section -->
<section id="aa-product">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="aa-product-area">
                        <div class="aa-product-inner">
                            <!-- start prduct navigation -->
                            <ul class="nav nav-tabs aa-products-tab">
                                <li class="active"><a href="#myaccount" data-toggle="tab">My Account</a></li>
                                <li><a href="#myorder" data-toggle="tab">My Order</a></li>
                                <li><a href="#addressbook" data-toggle="tab">Address Book</a></li>
                                {{--<li><a href="#clothes" data-toggle="tab">Clothes</a></li>--}}
                                {{--<li><a href="#electronics" data-toggle="tab">Infaq & Sadaqah</a></li>--}}
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <!-- Start men product category -->
                                <div class="tab-pane fade in active" id="myaccount" style="text-align: left;">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">{{ $user->first_name }}</h3>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-3 col-lg-3 " align="center"><img
                                                            alt="User Pic"
                                                            src="{{URL::asset('img/blank-avatar.png')}}"
                                                            class="img-circle img-responsive"></div>
                                                <div class=" col-md-9 col-lg-9 ">
                                                    <table class="table table-user-information">
                                                        <tbody>
                                                        <tr>
                                                            <td>Department:</td>
                                                            <td>Programming</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Email</td>
                                                            <td><a href="{{ $user->email }}">{{ $user->email }}</a>
                                                            </td>
                                                        </tr>

                                                        </tbody>
                                                    </table>

                                                    <!-- <a href="#" class="btn btn-primary">My Sales Performance</a>
                                                    <a href="#" class="btn btn-primary">Team Sales
                                                        Performance</a> -->
                                                </div>
                                            </div>
                                        </div>
                                        {{--<div class="panel-footer">--}}
                                        {{--<a data-original-title="Broadcast Message" data-toggle="tooltip"--}}
                                        {{--type="button" class="btn btn-sm btn-primary"><i--}}
                                        {{--class="glyphicon glyphicon-envelope"></i></a>--}}
                                        {{--<span class="pull-right">--}}
                                        {{--<a href="edit.html" data-original-title="Edit this user" data-toggle="tooltip"--}}
                                        {{--type="button"--}}
                                        {{--class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>--}}
                                        {{--<a data-original-title="Remove this user" data-toggle="tooltip" type="button"--}}
                                        {{--class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></a>--}}
                                        {{--</span>--}}
                                        {{--</div>--}}

                                    </div>
                                </div>
                                <!-- / men product category -->
                                <!-- start women product category -->
                                <div class="tab-pane fade" id="myorder">
                                    <div class=" col-md-9 col-lg-9">
                                        <table class="table table-hover table-bordered results"
                                               style="text-align: center">
                                            <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Amount</th>
                                                <th>Status</th>
                                                <th>Options</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($orders as $order)
                                                <tr>
                                                    <td>{{$order->id}}</td>
                                                    <td>{{$order->amount}}</td>
                                                    <td>{{$order->status}}</td>
                                                    <td>
                                                        @if($order->status === 1)
                                                            <form action="/cart/konfirmasipembayaran" method="POST">
                                                                {!! csrf_field() !!}
                                                                <input type="hidden" name="order_id"
                                                                       value="{{ $order->id }}">
                                                                <button class="aa-browse-btn"
                                                                        type="submit">Konfirmasi
                                                                    Pembayaran
                                                                </button>
                                                            </form>
                                                        @elseif($order->status === 2)
                                                            <button disabled="disabled" class="aa-browse-btn"
                                                                    type="submit">Menunggu
                                                                Konfirmasi Agen
                                                            </button>
                                                        @elseif($order->status === 3)
                                                            <button class="aa-browse-btn" type="submit">Input Data
                                                                Jamaah
                                                            </button>
                                                        @endif


                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                                <div class="tab-pane fade" id="addressbook">
                                    umrah
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- / Products section -->

@stop