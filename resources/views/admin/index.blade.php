@extends('adminlayouts.default')
@section('contentadmin')

    <div id="content" class="col-lg-10 col-sm-10">
        <div>
            <ul class="breadcrumb">
                <li>
                    <a href="#">Home</a>
                </li>
            </ul>
        </div>

        <div class="row">
            <div class="box col-md-12">
                <div class="box-inner">
                    <div class="box-header well" data-original-title="">
                        <h2><i class="glyphicon glyphicon-star-empty"></i> Your Title</h2>
                    </div>
                    <div class="box-content">

                        PUT YOUR CONTENT HERE!

                    </div>
                </div>
            </div>
        </div>
    </div><!--/#content.col-md-0-->

@endsection
