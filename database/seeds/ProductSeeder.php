<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('------- Product Seeder -------');
        foreach (range(1,10) as $index) {
            DB::table('products')->insert(array(
                array(
                    'category_id' => 1,
                    'name' => "Hajj " . $index,
                    'price' => '5000',
                    'description' => "This is the describtion of Hajj " . $index,
                    'stock' => 5,
                    'img' => ''
                ),
            ));

            DB::table('products')->insert(array(
                array(
                    'category_id' => 2,
                    'name' => "Umrah " . $index,
                    'description' => "This is the describtion of Umrah " . $index,
                    'price' => '4000',
                    'stock' => 5,
                    'img' => ''
                ),
            ));

            DB::table('products')->insert(array(
                array(
                    'category_id' => 3,
                    'name' => "Halal Food " . $index,
                    'description' => "This is the describtion of Halal Food " . $index,
                    'price' => '3000',
                    'stock' => 8,
                    'img' => ''
                ),
            ));

            DB::table('products')->insert(array(
                array(
                    'category_id' => 4,
                    'name' => "Clothes " . $index,
                    'description' => "This is the describtion of Clothes " . $index,
                    'price' => '3000',
                    'stock' => 9,
                    'img' => ''
                ),
            ));
        }
    }
}
